FROM registry.gitlab.com/candidguru/docker-base:0.2.1

ENV HOME /root

RUN set -x \
    && sh -c "curl -sL https://deb.nodesource.com/setup_6.x | bash -" \
    && apt-get update \
    && apt-get install -y imagemagick python-pip nodejs apt-transport-https ca-certificates python-dev git libfontconfig \
    && apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D \
    && echo "deb https://apt.dockerproject.org/repo ubuntu-xenial main" > /etc/apt/sources.list.d/docker.list \
    && curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
    && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list \
    && apt-get update \
    && apt-get purge lxc-docker \
    && apt-cache policy docker-engine \
    && apt-get install -y docker-engine yarn \
    && pip install awscli boto3 docker-compose \
    && curl -L https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-2.1.1-linux-x86_64.tar.bz2 | tar -jx \
    && mv phantomjs-2.1.1-linux-x86_64/bin/phantomjs /usr/local/bin \
    && rm -rf phantomjs-2.1.1-linux-x86_64/ \
    && mkdir -p /opt/images \
    && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

COPY entrypoint.sh /usr/local/bin/
COPY docker-run.sh /etc/my_init.d/

ENTRYPOINT ["/sbin/my_init", "--", "entrypoint.sh"]
