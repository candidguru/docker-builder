#!/usr/bin/env sh

docker login -u candidguru_deployer -p $DEPLOYER_AUTH $CI_REGISTRY

REF_TAG=$CI_REGISTRY_IMAGE:$CI_BUILD_REF

# Does the image already exist?
docker pull $REF_TAG
FOUND_REF_TAG=$?

set -e

if [ $FOUND_REF_TAG -ne 0 ]; then

    # Image does not exist, so build it
    echo "Could not find $REF_TAG so building it..."
    docker build -t $REF_TAG .

    SHOULD_PUSH="true"
fi

if [ -n "$CI_BUILD_TAG" ]; then
    echo "Tagging build with $CI_BUILD_TAG"
    docker tag $REF_TAG $CI_REGISTRY_IMAGE:$CI_BUILD_TAG

    SHOULD_PUSH="true"
fi

if [ -n "$SHOULD_PUSH" ]; then
    echo "Pushing the image..."
    docker login -u gitlab-ci-token -p "$CI_BUILD_TOKEN" $CI_REGISTRY
    docker push $CI_REGISTRY_IMAGE
fi
