#!/bin/bash

set -e

echo "Executing entrypoint.sh"

# if we have "--link some-docker:docker" and not DOCKER_HOST, let's set DOCKER_HOST automatically
if [ -z "$DOCKER_HOST" -a "$DOCKER_PORT_2375_TCP" ]; then
    echo "Setting DOCKER_HOST to linked container"
	export DOCKER_HOST='tcp://docker:2375'
fi

exec "$@"
